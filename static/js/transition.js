document.addEventListener("DOMContentLoaded", function () {
    const imageContainer = document.getElementById("imageContainer");
    const image1 = document.getElementById("image1");
    const image2 = document.getElementById("image2");

    setInterval(() => {
        if (image1.style.opacity === "0") {
            image1.style.opacity = "1";
            image2.style.opacity = "0";
        } else {
            image1.style.opacity = "0";
            image2.style.opacity = "1";
        }
    }, 5000);
});
