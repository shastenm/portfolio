from django.urls import path
from . import views

urlpatterns = [ 
    path('', views.index, name='my_home'),
    path('about/', views.about, name='about'),
    path('resume/', views.resume, name="my_resume"),
    path('contact/', views.contact_view, name="contact_view"),
]
