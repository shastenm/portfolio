from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.conf import settings
from .forms import ContactForm

def index(request):
    return render(request, 'index/index.html')

def resume(request):
    return render(request, 'index/resume.html', {})

def about(request):
    return render(request, 'index/index.html')

def contact_view(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()

            subject_admin = 'New Contact Form Submission'
            message_admin = f'Name: {form.cleaned_data["name"]}\nEmail: {form.cleaned_data["email"]}\n\n{form.cleaned_data["message"]}'

            send_mail(subject_admin, message_admin, settings.DEFAULT_FROM_EMAIL, [settings.EMAIL.HOST.USER])

            return render(request, 'index/success.html')

    else:  # Use else to ensure form is initialized for GET requests
        form = ContactForm()

    context = {'form': form}
    return render(request, 'index/index.html', context)

